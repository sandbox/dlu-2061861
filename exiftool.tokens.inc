<?php

/**
 * @file
 * Builds placeholder replacement tokens for image metadata.
 */

/**
 * Implements hook_token_info().
 */
function exiftool_token_info() {

  DPM('exiftool_token_info()');
  /*
   * Data types.
   */

  $types['exif'] = array(
    'name' => t('EXIF'),
    'description' => t('EXIF (camera generated) metadata.'),
    'needs-data' => 'exif',
  );

  $types['gps'] = array(
    'name' => t('GPS'),
    'description' => t('GPS metadata.'),
    'needs-data' => 'gps',
  );

  $types['iptc'] = array(
    'name' => t('IPTC'),
    'description' => t('IPTC (user generated) metadata.'),
    'needs-data' => 'iptc',
  );

  /*
   * EXIF fields.
   */

  $exif['make'] = array(
    'name' => t('Make'),
    'description' => t("The camera maker."),
  );

  $exif['camera-model-name'] = array(
    'name' => t('CameraModelName'),
    'description' => t("The camera model."),
  );

  $exif['date-time-original'] = array(
    'name' => t('DateTimeOriginal'),
    'description' => t("The capture time of the image."),
  );

  $exif['aperture'] = array(
    'name' => t('Make'),
    'description' => t("Aperture."),
  );

  $exif['iso'] = array(
    'name' => t('iso'),
    'description' => t("ISO sensitivity."),
  );

  $exif['shutter-speed'] = array(
    'name' => t('ShutterSpeed'),
    'description' => t("Shutter speed."),
  );

  /*
   * GPS fields.
   */

  $gps['alt'] = array(
    'name' => t('Altitude'),
    'description' => t("Altitude"),
  );

  $gps['lat'] = array(
    'name' => t('Lattitude'),
    'description' => t("Lattitude"),
  );

  $gps['lon'] = array(
    'name' => t('Longitude'),
    'description' => t("Longitude"),
  );

  $gps['time'] = array(
    'name' => t('Time'),
    'description' => t("GPS time stamp"),
  );

  /*
   * IPTC fields.
   */

  $iptc['caption'] = array(
    'name' => t('Caption'),
    'description' => t("Caption"),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'exif' => $exif,
      'gps' => $gps,
      'iptc' => $iptc,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function exiftool_tokens($type, $tokens, array $data = array(), array $options = array()) {

  DPM($data, 'exiftool_tokens()');

  $token_service = Drupal::token();
  $url_options = array('absolute' => TRUE);
  if (isset($options['langcode'])) {
    $url_options['language'] = language_load($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'exif' && !empty($data['file'])) {
    $exif = $data['exif'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Basic user account information.
        case 'uid':
          // In the case of hook user_presave uid is not set yet.
          $replacements[$original] = $account->id() ?: t('not yet assigned');
          break;

        case 'name':
          $name = user_format_name($account);
          $replacements[$original] = $sanitize ? check_plain($name) : $name;
          break;

        case 'mail':
          $replacements[$original] = $sanitize ? check_plain($account->getEmail()) : $account->getEmail();
          break;

        case 'url':
          $replacements[$original] = $account->id() ? url("user/" . $account->id(), $url_options) : t('not yet assigned');
          break;

        case 'edit-url':
          $replacements[$original] = $account->id() ? url("user/" . $account->id() . "/edit", $url_options) : t('not yet assigned');
          break;

        // These tokens are default variations on the chained tokens handled below.
        case 'last-login':
          $replacements[$original] = $account->getLastLoginTime() ? format_date($account->getLastLoginTime(), 'medium', '', NULL, $langcode) : t('never');
          break;

        case 'created':
          // In the case of user_presave the created date may not yet be set.
          $replacements[$original] = $account->getCreatedTime() ? format_date($account->getCreatedTime(), 'medium', '', NULL, $langcode) : t('not yet created');
          break;
      }
    }

    if ($login_tokens = $token_service->findWithPrefix($tokens, 'last-login')) {
      $replacements += $token_service->generate('date', $login_tokens, array('date' => $account->getLastLoginTime()), $options);
    }

    if ($registered_tokens = $token_service->findWithPrefix($tokens, 'created')) {
      $replacements += $token_service->generate('date', $registered_tokens, array('date' => $account->getCreatedTime()), $options);
    }
  }

  return $replacements;
}
