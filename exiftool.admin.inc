<?php

/**
 * @file
 * EXIFtool module administrative options.
 *
 */

function exiftool_settings($form_state) {
  $exiftool_system_settings = array(

    "exiftool_command" => array(
      "#required" => TRUE,
      "#type" => "textfield",
      "#title" => "Absolute path to the exiftool command",
      "#description" => "The absolute path to the version of exiftool to use with the module.",
      "#default_value" => variable_get('exiftool_command',
                                       "/usr/bin/exiftool"),
    ),

    "exiftool_std_args" => array(
      "#required" => TRUE,
      "#type" => "textfield",
      "#title" => "Standard arguments to exiftool",
      "#description" => "The standard (required by the module) arguments to the exiftool command. You don't want to change these unless you have a good reason (and know what you are doing).",
      "#default_value" => variable_get('exiftool_std_args',
                                       "-json -f -d '%d.%m.%Y'"),
    ),

    "exiftool_field_list" => array(
      "#required" => TRUE,
      "#type" => "textfield",
      "#title" => "Field List",
      "#description" => "The name of the file, stored in field list directory, which holds the list of metadata fields for exiftool to extract.",
      "#default_value" => variable_get("exiftool_args",
                                       "exiftool.arglist"),
    ),

    "imagestore" => array(
      "#required" => TRUE,
      "#type" => "textfield",
      "#title" => "Image Store Path",
      "#description" => "The location of the images that the module will extract metadata from. This is, currently, site wide.",
      "#default_value" => variable_get("imagestore",
                                       "sites/canishe.com/files/photos"),
    ),

    "exiftool_field_list_dir" => array(
      "#required" => TRUE,
      "#type" => "textfield",
      "#title" => "Field list directory",
      "#description" => "The directory where the field list file is stored. This could be anywhere that Drupal can read, the default field list is installed in the module diretory.",
      "#default_value" => variable_get("exiftool_field_list_dir",
                                       "sites/all/modules/exiftool"),
    ),
  );

  return system_settings_form($exiftool_system_settings);
}

/*
function exiftool_setings_verify() {
}
*/