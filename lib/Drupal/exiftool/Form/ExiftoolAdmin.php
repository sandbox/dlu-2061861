<?php

/**
 * @file
 * Contains \Drupal\exiftool\Form\ExiftoolAdmin.
 */

namespace Drupal\exiftool\Form;

use Drupal;
use Drupal\Core\Controller\ControllerInterface;
use Drupal\Core\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Exiftool
 */
class ExiftoolAdmin implements FormInterface, ControllerInterface {

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('exiftool.admin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'exiftool_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param string $default_ip
   *   (optional) IP address to be passed on to drupal_get_form() for use as the
   *   default value of the IP address form field.
   */
  public function buildForm(array $form, array &$form_state, Request $request = NULL) {
    $this->request = $request;

    $path = Drupal::config('exiftool.settings')->get('path');

    $form['path'] = array(
      '#title' => t('Location of Exiftool'),
      '#description' => t('The fully qualified path of the exiftool executable on the server.'),
      '#required' => TRUE,
      '#type' => 'textfield',
      '#size' => 48,
      '#maxlength' => 128,
      '#default_value' => $path,
    );

    $form['args'] = array(
      '#title' => 'Standard arguments to exiftool',
      '#description' => 'The standard (required by the module) arguments to the exiftool command. You don\'t want to change these unless you have a good reason (and know what you are doing).',
      '#required' => TRUE,
      '#type' => 'textfield',
      '#size' => 48,
      '#maxlength' => 128,
      '#default_value' => Drupal::config('exiftool.settings')->get('args'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
    $form['actions']['restore'] = array(
      '#type' => 'submit',
      '#value' => t('Restore default configuration'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    //DPM($form_state);
    $config = Drupal::config('exiftool.settings');
    if ($form_state['values']['op'] == 'Save configuration') {
      $config->set('args', $form_state['values']['args'])->save();
      $config->set('path', $form_state['values']['path'])->save();
      drupal_set_message('The configuration options have been saved.');
    }
    else {
      // Reset to module defaults.

      $config->set('args', $config->get('args_default'))->save();
      $config->set('path', $config->get('path_default'))->save();

      drupal_set_message('The default configuration options have been restored.');
    }
  }

}
